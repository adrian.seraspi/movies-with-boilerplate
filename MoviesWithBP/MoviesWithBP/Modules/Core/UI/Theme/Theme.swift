//
//  Theme.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

// swiftlint:disable type_name

import UIKit

typealias T = Theme

struct Theme {
  struct color {}
  struct font {}
  struct textStyle {}
}
