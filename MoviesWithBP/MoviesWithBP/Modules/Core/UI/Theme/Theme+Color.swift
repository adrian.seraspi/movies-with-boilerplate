//
//  Theme+Color.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

// swiftlint:disable type_name

import UIKit

extension Theme.color {
  static let primaryFull: UIColor = R.color.primary_Full()!
  static let primaryShade1: UIColor = R.color.primary_Shade1()!
  static let primaryShade2: UIColor = R.color.primary_Shade2()!
  static let secondaryFull: UIColor = R.color.secondary_Full()!

  static let error: UIColor = R.color.error()!
  static let success: UIColor = R.color.success()!
  static let warning: UIColor = R.color.warning()!

  static let disabledLight: UIColor = R.color.disabled_Light()!
  static let disabledDark: UIColor = R.color.disabled_Dark()!

  static let backgroundLight: UIColor = R.color.background_Light()!
  static let backgroundDark: UIColor = R.color.background_Dark()!

  static let sheetLight: UIColor = R.color.sheet_Light()!
  static let sheetDark: UIColor = R.color.sheet_Dark()!

  static let textOnLightRegular: UIColor = R.color.text_OnLight_Regular()!
  static let textOnLightSecondary: UIColor = R.color.text_OnLight_Secondary()!
  static let textOnLightInactive: UIColor = R.color.text_OnLight_Inactive()!

  static let textOnPrimaryRegular: UIColor = R.color.text_OnPrimary_Regular()!
  static let textOnDarkRegular: UIColor = R.color.text_OnDark_Regular()!
  static let textOnDarkSecondary: UIColor = R.color.text_OnDark_Secondary()!
  static let textOnDarkInactive: UIColor = R.color.text_OnDark_Inactive()!
}
