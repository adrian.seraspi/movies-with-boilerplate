//
//  MovieSearchRepository.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/18/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol MoviePersistRepositoryProtocol {
  func addNewEntry(_ query: String)
  func getSearch() -> String
}
