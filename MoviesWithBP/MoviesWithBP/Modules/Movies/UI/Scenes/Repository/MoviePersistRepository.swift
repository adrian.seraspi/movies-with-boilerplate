//
//  MovieSearchRepository.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/18/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class MoviePersistRepository: MoviePersistRepositoryProtocol {
  private let userDefaults: UserDefaults
  private let key = "\(String(describing: MoviePersistRepository.self)).Key"
  
  init(userDefaults: UserDefaults = UserDefaults.standard) {
    self.userDefaults = userDefaults
  }
}

extension MoviePersistRepository {
  func addNewEntry(_ query: String) {
    userDefaults.setValue(query, forKey: key)
  }
  
  func getSearch() -> String {
    return userDefaults.string(forKey: key) ?? ""
  }
}
