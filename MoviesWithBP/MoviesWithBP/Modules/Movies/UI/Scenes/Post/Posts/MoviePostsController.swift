//
//  MoviePostsViewController.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/13/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Material
import UIKit

class MoviePostsController: ViewController {
  var viewModel: MoviePostsViewModelProtocol!
  
  @IBOutlet private(set) var tableView: UITableView!
  @IBOutlet private(set) var refreshView: UIActivityIndicatorView!
  
}

// MARK: - Lifecycle

extension MoviePostsController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(false, animated: true)
  }
}

// MARK: - Setup

private extension MoviePostsController {
  func setup() {
    setupTableView()
    setupPersistData()
    setupRefresh()
  }
  
  func setupTableView() {
    tableView.register(UINib(resource: R.nib.movieCell),
                       forCellReuseIdentifier: R.nib.movieCell.name)
    if viewModel == nil {
      viewModel = MoviePostsViewModel()
    }
  }
  
  func setupPersistData() {
    viewModel.loadPersistenceData(onSuccess: handleLoadListSuccess(),
                                  onFailed: handleLoadListError())
  }
  
  func setupRefresh() {
    viewModel.onRefresh = { self.onRefresh(isRefreshing: $0) }
  }
}

// MARK: - Actions

extension MoviePostsController {
  func search(query: String) {
    viewModel.searchMoviePosts(query: query,
                               onSuccess: handleLoadListSuccess(),
                               onFailed: handleLoadListError())
  }
  
  func onRefresh(isRefreshing: Bool) {
    if isRefreshing {
      self.refreshView.startAnimating()
    } else {
      self.refreshView.stopAnimating()
    }
  }
}

// MARK: - Event Handlers

private extension MoviePostsController {
  func handleLoadListSuccess() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.refreshView.stopAnimating()
      self.refresh()
    }
  }

  func handleLoadListError() -> ErrorResult {
    return { [weak self] error in
      guard let self = self else { return }
      self.refreshView.stopAnimating()
      self.infoPresenter.presentErrorInfo(error: error)
    }
  }
}

// MARK: - Refresh

private extension MoviePostsController {
  @objc
  func refresh() {
    tableView.reloadData()
  }
}

// MARK: - Routing

private extension MoviePostsController {
  func presentMovieDetails(at indexPath: IndexPath) {
    let vc = R.storyboard.movieListPost.movieDetailsController()!
    vc.viewModel = viewModel.generateMovieDetailsVM(at: indexPath.row)
    navigationController?.pushViewController(vc, animated: true)
  }
}

// MARK: - UICollectionViewDelegate

extension MoviePostsController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      presentMovieDetails(at: indexPath)
  }
}

// MARK: - UICollectionViewDataSource

extension MoviePostsController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    viewModel.datasource.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(
      withIdentifier: R.nib.movieCell.name,
      for: indexPath
    ) as? MovieCell,
      let movieCellViewModel = viewModel.datasource[safe: indexPath.row]
    else {
      return MovieCell()
    }

    cell.viewModel = movieCellViewModel

    return cell
  }
}
