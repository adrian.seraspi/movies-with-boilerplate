//
//  MoviePostsViewModelProtocol.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/13/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol MoviePostsViewModelProtocol {
  var onRefresh: BoolResult? { set get }

  var datasource: [MovieCellViewModelProtocol] { get }
  
  func searchMoviePosts(
    query: String,
    onSuccess: @escaping VoidResult,
    onFailed: @escaping ErrorResult
  )
  
  func loadPersistenceData(onSuccess: @escaping VoidResult,
                           onFailed: @escaping ErrorResult)
  
  func generateMovieDetailsVM(at index: Int) -> MovieDetailsViewModelProtocol
}
