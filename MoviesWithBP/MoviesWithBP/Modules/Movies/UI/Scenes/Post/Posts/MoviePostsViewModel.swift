//
//  MoviePostsViewModel.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/13/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class MoviePostsViewModel: MoviePostsViewModelProtocol {
  var onRefresh: BoolResult?
  
  var datasource: [MovieCellViewModelProtocol] = []
  var posts: [MainMovies] = []
  private let service: MovieServiceProtocol
  private let repo: MoviePersistRepositoryProtocol
  
  init(service: MovieServiceProtocol = App.shared.movie,
       repo: MoviePersistRepositoryProtocol = MoviePersistRepository()) {
    self.service = service
    self.repo = repo
  }
}

// MARK: - Methods: Fetch

extension MoviePostsViewModel {
  func searchMoviePosts(query: String,
                        onSuccess: @escaping VoidResult,
                        onFailed: @escaping ErrorResult) {
    onRefresh?(true)
    service.fetchSearchMovie(query: query,
                             country: "au",
                             onSuccess: handleSuccess(query: query,
                                                      thenExecute: onSuccess),
                             onError: handleError(thenExecute: onFailed))
  }
  
  func loadPersistenceData(onSuccess: @escaping VoidResult,
                           onFailed: @escaping ErrorResult) {
    onRefresh?(true)
    service.getMoviesFromLocal(onSuccess: handleSuccess(thenExecute: onSuccess),
                               onError: handleError(thenExecute: onFailed))
  }
  
  func generateMovieDetailsVM(at index: Int) -> MovieDetailsViewModelProtocol {
    return MovieDetailsViewModel(movie: posts[index])
  }
}

// MARK: - Handlers

private extension MoviePostsViewModel {
  func handleSuccess(query: String = "",
                     thenExecute handler: @escaping VoidResult) -> SingleResult<[MainMovies]> {
    return { [weak self] newMovies in
      guard let self = self else { return }
      
      if !query.isEmpty { self.repo.addNewEntry(query) }
      
      self.posts = newMovies
      self.datasource = self.posts.map { MovieCellViewModel(mainMovies: $0) }
      
      handler()
      self.onRefresh?(false)
    }
  }
  
  func handleError(thenExecute handler: @escaping ErrorResult) -> ErrorResult {
    return { [weak self] error in
      guard let self = self else { return }
      self.onRefresh?(false)
      handler(error)
    }
  }
}
