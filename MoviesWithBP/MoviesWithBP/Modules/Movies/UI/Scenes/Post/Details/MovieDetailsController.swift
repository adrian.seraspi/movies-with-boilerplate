//
//  
//  MovieDetailsViewController.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/17/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//
//

import UIKit
import AVKit

class MovieDetailsController: ViewController {
  var viewModel: MovieDetailsViewModelProtocol?
  var player: AVPlayer?
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var genreLabel: UILabel!
  @IBOutlet weak var longDescriptionLabel: UILabel!
  @IBOutlet weak var previewContainerView: UIView!
  @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
}

// MARK: - Lifecycle

extension MovieDetailsController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear(animated)
      player?.pause()
  }
  
  override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      if player == nil {
        setupPreview()
      }
  }
}

// MARK: - Setup

private extension MovieDetailsController {
  
  func setup() {
    setupLabels()
    setupPreview()
  }
  
  func setupLabels() {
    titleLabel.text = viewModel?.title
    genreLabel.text = viewModel?.genre
    longDescriptionLabel.text = viewModel?.longDescription
  }
  
  func setupPreview() {
    if viewModel!.hasPreview {
      buildVideoPlayer()
    } else {
      buildImageView()
    }
  }
  
  func buildImageView() {
    let imageView = UIImageView()
    imageView.contentMode = .scaleAspectFill
    imageView.setImageWithURL(viewModel?.imageUrl,
                              placeholder: R.image.thumbnailPlaceholder())
    imageView.frame = previewContainerView.frame
    previewContainerView.addSubview(imageView)
  }
  
  func buildVideoPlayer() {
    //  Add on tap gesture on player container view
    let gesture = UITapGestureRecognizer(target: self,
                                         action: #selector(onPlayerPressed(_:)))
    previewContainerView.addGestureRecognizer(gesture)
    
    //  Create a video player
    player = AVPlayer(url: (viewModel?.previewUrl)!)
    let playerLayer = AVPlayerLayer(player: player)
    playerLayer.videoGravity = .resizeAspectFill
    playerLayer.frame = previewContainerView.bounds
    previewContainerView.layer.addSublayer(playerLayer)
    _ = setupAVPlayerObserver()
    player?.play()
  }
  
}

// MARK: - Actions

private extension MovieDetailsController {
  @objc func onPlayerPressed(_: UITapGestureRecognizer) {
      guard let player = player else { return }
      
      //  Check if player is playing
      if (player.rate != 0) && (player.error == nil) {
          player.pause()
      } else {
          player.play()
      }
  }
}

// MARK: - Observers

private extension MovieDetailsController {
  
  func setupAVPlayerObserver() -> NSKeyValueObservation? {
    return player?.observe(\.timeControlStatus,
                           options: [.old, .new],
                           changeHandler: { _, change in
                             
                             guard let newValue = change.newValue,
                                   let oldValue = change.oldValue else {
                               return
                             }
                             self.updateLoadingIndicator(oldStatus: oldValue,
                                                    newStatus: newValue)
                   })
  }
  
  func updateLoadingIndicator(oldStatus: AVPlayer.TimeControlStatus,
                              newStatus: AVPlayer.TimeControlStatus) {
    if newStatus != oldStatus {
        DispatchQueue.main.async {[weak self] in
            if newStatus == .playing || newStatus == .paused {
                self?.loadingIndicator.isHidden = true
            } else {
                self?.loadingIndicator.isHidden = false
            }
        }
    }
  }
}
