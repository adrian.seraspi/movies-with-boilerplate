//
//  
//  Contract.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/17/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//
//

import Foundation

protocol MovieDetailsViewModelProtocol {
  var title: String { get }
  var genre: String { get }
  var longDescription: String { get }
  var previewUrl: URL? { get }
  var imageUrl: URL? { get }
  var hasPreview: Bool { get }
}
