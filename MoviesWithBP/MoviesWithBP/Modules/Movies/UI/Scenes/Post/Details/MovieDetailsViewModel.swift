//
//  
//  MovieDetailsViewModel.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/17/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//
//

import UIKit

class MovieDetailsViewModel: MovieDetailsViewModelProtocol {
  
  private let movie: MainMovies
  
  init(movie: MainMovies) {
    self.movie = movie
  }
}

extension MovieDetailsViewModel {
  var title: String { movie.trackName }
  var genre: String { movie.genre }
  var longDescription: String { movie.longDescription }
  var previewUrl: URL? { URL(string: movie.previewUrl) }
  var imageUrl: URL? { URL(string: movie.imageUrl) }
  var hasPreview: Bool {
    if previewUrl != nil {
      return true
    }
    return false
  }
}
