//
//  MovieListMainViewModelProtocol.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/18/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol MovieListMainViewModelProtocol {
  var persistSearch: String { get }
}
