//
//  MovieListController.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/12/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class MovieListMainController: ViewController {

  @IBOutlet private(set) var searchBar: UISearchBar!
  
  private var viewModel: MovieListMainViewModelProtocol!
  private var embeddedViewController: MoviePostsController?
}

// MARK: - Lifecycle

extension MovieListMainController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let vc = segue.destination as? MoviePostsController {
      embeddedViewController = vc
    }
  }
}

// MARK: - Setup
private extension MovieListMainController {
  func setup() {
    setupTitle()
    setupSearchBar()
    setupViewModel()
  }
  
  func setupTitle() {
    let titleLabel = UILabel()
    titleLabel.text = S.appName()
    titleLabel.font = R.font.sfProDisplayBold(size: 28)
    navigationItem.leftBarButtonItem = UIBarButtonItem(customView: titleLabel)

    // Remove underline and shadow in navigation bar
    navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    navigationController?.navigationBar.shadowImage = UIImage()
    navigationController?.navigationBar.layoutIfNeeded()
  }
  
  func setupSearchBar() {
    searchBar.delegate = self
  }
  
  func setupViewModel() {
    viewModel = MovieListMainViewModel()
    searchBar.text = viewModel.persistSearch
  }
}

// MARK: - UISearchBarDelegate

extension MovieListMainController: UISearchBarDelegate {
  
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    searchBar.endEditing(true)
    
    guard
      let searchTerm = searchBar.text,
      !searchTerm.isEmpty
    else { return }
    
    embeddedViewController?.search(query: searchTerm)
  }
}
