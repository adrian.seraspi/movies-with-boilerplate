//
//  MovieListMainViewModel.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/18/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class MovieListMainViewModel: MovieListMainViewModelProtocol {
  private let repo: MoviePersistRepositoryProtocol
  
  init(repo: MoviePersistRepositoryProtocol = MoviePersistRepository()) {
    self.repo = repo
  }
}

extension MovieListMainViewModel {
  var persistSearch: String { repo.getSearch() }
}
