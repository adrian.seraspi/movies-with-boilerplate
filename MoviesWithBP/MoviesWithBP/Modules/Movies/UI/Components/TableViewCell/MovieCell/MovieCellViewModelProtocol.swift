//
//  MoviesCellViewModelProtocol.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/13/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol MovieCellViewModelProtocol {
  var titleText: String { get }
  var priceText: String { get }
  var genreText: String { get }
  var movieImageURL: URL? { get }
}
