//
//  MoviesCell.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/13/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {
  var viewModel: MovieCellViewModelProtocol! {
    didSet { setupVM() }
  }
  
  @IBOutlet private(set) var titleLabel: UILabel!
  @IBOutlet private(set) var priceLabel: UILabel!
  @IBOutlet private(set) var genreLabel: UILabel!
  @IBOutlet private(set) var movieImageView: UIImageView!
}

private extension MovieCell {
  func setupVM() {
    guard viewModel != nil else { return }
    
    titleLabel.text = viewModel.titleText
    priceLabel.text = viewModel.priceText
    genreLabel.text = viewModel.genreText
    movieImageView.setImageWithURL(viewModel.movieImageURL,
                                   placeholder: R.image.thumbnailPlaceholder())
  }
}
