//
//  MoviesCellViewModel.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/13/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class MovieCellViewModel: MovieCellViewModelProtocol {  
  
  private var mainMovies: MainMovies
  
  init(mainMovies: MainMovies) {
    self.mainMovies = mainMovies
  }
}

//  MARK: Getters
extension MovieCellViewModel {
  var titleText: String { mainMovies.trackName }
  var priceText: String { "\(mainMovies.currency) \(mainMovies.trackPrice)" }
  var genreText: String { mainMovies.genre }
  var movieImageURL: URL? { URL(string: mainMovies.imageUrl) }
}
