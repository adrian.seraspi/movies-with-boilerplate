//
//  ITunesMovieList.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/14/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct ITunesMovieList: APIModel, Codable {
  var id: Int = 0
  var imageUrl: String = ""
  var previewUrl: String = ""
  var trackName: String = ""
  var trackPrice: Double = 0.0
  var currency: String = ""
  var genre: String = ""
  var longDescription: String = ""
  
  enum CodingKeys: String, CodingKey {
      case id = "trackId"
      case imageUrl = "artworkUrl100"
      case previewUrl
      case trackName
      case trackPrice
      case currency
      case genre = "primaryGenreName"
      case longDescription
  }
}

extension ITunesMovieList {
    
    func convertToMainMovies(newId: Int) -> MainMovies {
        return MainMovies(id: newId,
                          imageUrl: self.imageUrl,
                          previewUrl: self.previewUrl,
                          trackName: self.trackName,
                          trackPrice: self.trackPrice,
                          currency: self.currency,
                          genre: self.genre,
                          longDescription: self.longDescription)
    }
    
}
