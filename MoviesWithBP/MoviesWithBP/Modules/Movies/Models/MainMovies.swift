//
//  MainMovies.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/13/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct MainMovies: Codable, Equatable {
    var id: Int = -1
    var imageUrl: String = ""
    var previewUrl: String = ""
    var trackName: String = ""
    var trackPrice: Double = 0.0
    var currency: String = ""
    var genre: String = ""
    var longDescription: String = ""
}
