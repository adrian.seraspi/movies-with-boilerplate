//
//  MoviesAPI.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/14/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol MoviesAPI {
  @discardableResult
  func getSearchMovies(
    query: String,
    country: String,
    onSuccess: @escaping SingleResult<[ITunesMovieList]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol
}
