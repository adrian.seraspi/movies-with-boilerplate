//
//  APIClient+Movies.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/14/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

extension APIClient: MoviesAPI {
  func getSearchMovies(query: String,
                       country: String,
                       onSuccess: @escaping SingleResult<[ITunesMovieList]>,
                       onError: @escaping ErrorResult) -> RequestProtocol {
    let params = [
      "term": query,
      "country": country,
      "media": "movie"
    ]
    return request("search",
                   parameters: params,
                   success: decodeModel(onSuccess: onSuccess, onError: onError),
                   failure: onError)
  }
}
