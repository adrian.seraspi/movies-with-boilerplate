//
//  LocalDatabaseProtocol.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/17/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol LocalDatabaseProtocol {
    
  //  A request to save all movie in database
  func saveAllMovies(list: [MainMovies])
    
  //  A request to get all movies from the database
  func getAllMovies(onSuccess: @escaping SingleResult<[MainMovies]>,
                    onError: @escaping ErrorResult)
    
  //  A request to delete all items in the database
  func deleteAllMovies()
    
}
