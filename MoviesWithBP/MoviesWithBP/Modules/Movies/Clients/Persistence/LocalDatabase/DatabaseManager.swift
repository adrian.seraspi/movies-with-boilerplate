//
//  DatabaseManager.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/17/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import CoreData

class DatabaseManager: LocalDatabaseProtocol {
    
  private var managedObjectContext: NSManagedObjectContext!
  private var persistentContainer: NSPersistentContainer!
    
  init(persistentContainer: NSPersistentContainer) {
    self.persistentContainer = persistentContainer
    self.managedObjectContext = persistentContainer.viewContext
  }
  
  func getAllMovies(onSuccess: @escaping SingleResult<[MainMovies]>,
                    onError: @escaping ErrorResult) {
    self.managedObjectContext.perform {
      do {
        //  Sort the movie entity by id in ascending order
        let request: NSFetchRequest<Movie> = Movie.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]

        //  Request for list of movie entity and convert it to Movie
        let movieEntityList = try self.managedObjectContext.fetch(request) as [Movie]
        let listOfMainMovies = movieEntityList.map { $0.convertToMainMovies() }
        onSuccess(listOfMainMovies)
      } catch let error {
        onError(error)
      }
      self.managedObjectContext.reset()
    }
  }
    
  func saveAllMovies(list: [MainMovies]) {
    self.managedObjectContext.performAndWait {
      for item in list {
        // Get Movie Entity
        let movie = NSEntityDescription.insertNewObject(
          forEntityName: "Movie",
          into: self.managedObjectContext)

        movie.setValue(item.id, forKey: "id")
        movie.setValue(item.currency, forKey: "currency")
        movie.setValue(item.genre, forKey: "genre")
        movie.setValue(item.imageUrl, forKey: "imageUrl")
        movie.setValue(item.longDescription, forKey: "longDescription")
        movie.setValue(item.trackName, forKey: "name")
        movie.setValue(item.previewUrl, forKey: "previewUrl")
        movie.setValue(item.trackPrice, forKey: "trackPrice")
      }

      // Save the object database
      do {
        try self.managedObjectContext.save()
      } catch {
        print(error)
      }
      // Reset the context to clear up cache
      self.managedObjectContext.reset()
    }
  }
    
  func deleteAllMovies() {
    self.managedObjectContext.performAndWait {
      //  Retrieve fetch request of movie entity
      do {
        //  Request for list of movie entity and cast to NSManagedObject
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: Movie.fetchRequest())
        try self.managedObjectContext.execute(batchDeleteRequest)
      } catch let error {
        print(error)
      }
      //  Reset the context to clear up cache
      self.managedObjectContext.reset()
    }
  }
}
