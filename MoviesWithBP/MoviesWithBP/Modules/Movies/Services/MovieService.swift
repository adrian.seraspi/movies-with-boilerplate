//
//  MovieService.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/14/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class MovieService: MovieServiceProtocol {
  
  private let api: MoviesAPI
  private let localDb: LocalDatabaseProtocol
  
  init(api: MoviesAPI, localDb: LocalDatabaseProtocol) {
    self.api = api
    self.localDb = localDb
  }
}

extension MovieService {
  func fetchSearchMovie(query: String,
                        country: String,
                        onSuccess: @escaping SingleResult<[MainMovies]>,
                        onError: @escaping ErrorResult) {
    api.getSearchMovies(query: query,
                        country: country,
                        onSuccess: handleSuccess(thenExecute: onSuccess),
                        onError: handleError(thenExecute: onError))
  }
  
  func getMoviesFromLocal(onSuccess: @escaping SingleResult<[MainMovies]>,
                          onError: @escaping ErrorResult) {
    localDb.getAllMovies(onSuccess: onSuccess, onError: onError)
  }
}

extension MovieService {
  
  func handleSuccess(thenExecute handler: @escaping SingleResult<[MainMovies]>) -> SingleResult<[ITunesMovieList]> {
    return { [weak self] newMovies in
      guard let self = self else { return }
      
      //  Convert the MovieList.Result to MainMovie
      let listOfMainMovies = newMovies.enumerated().map {
        //  Setting the index as id
        //  This will be used in getting the list from the database
        //  Database will sort it by id to keep the position of the list
        $0.element.convertToMainMovies(newId: $0.offset)
      }
      
      //  Delete all movies in the database
      self.localDb.deleteAllMovies()
      
      //  Save a new list in database
      self.localDb.saveAllMovies(list: listOfMainMovies)
      
      handler(listOfMainMovies)
    }
  }
  
  func handleError(thenExecute handler: @escaping ErrorResult) -> ErrorResult {
    return { [weak self] error in
      guard self != nil else { return }
      handler(error)
    }
  }
  
}
