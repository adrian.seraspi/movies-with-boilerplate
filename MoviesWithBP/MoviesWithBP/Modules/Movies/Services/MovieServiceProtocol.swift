//
//  MovieServiceProtocol.swift
//  MoviesWithBP
//
//  Created by Adrian Jun Seraspi on 5/14/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol MovieServiceProtocol {
  func fetchSearchMovie(query: String,
                        country: String,
                        onSuccess: @escaping SingleResult<[MainMovies]>,
                        onError: @escaping ErrorResult)
  
  func getMoviesFromLocal(onSuccess: @escaping SingleResult<[MainMovies]>,
                          onError: @escaping ErrorResult)
}
