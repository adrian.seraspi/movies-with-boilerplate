//
//  SessionServiceProtocol.swift
//  example
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol SessionServiceProtocol: AppServiceProtocol {
  var onResumeSessionError: ErrorResult? { get set }
  
  var isActive: Bool { get }
  var user: User? { get }
  var hasSkippedEmailVerification: Bool { get }

  func handleUserResult() -> SingleResult<User>
  func handleAvatarResult() -> DoubleResult<Photo, Data>
  func handleDeAuth() -> BoolResult
  func handleDeAuthError() -> ErrorResult
  func handleUnauthorizedError() -> VoidResult
  
  func clearSession(shouldBroadcast: Bool)
  func recordEmailVerificationSkip()
  func recordAddOTPEmailComplete()
}
