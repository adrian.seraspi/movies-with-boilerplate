//
//  FilterCountButton.swift
//  example
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class FilterCountButton: UIButton {
  var count: Int = 0 {
    didSet { didSetCount() }
  }

  private var countLabel: UILabel!
  private let labelWidth: CGFloat = 16

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: - Setup

private extension FilterCountButton {
  func setup() {
    setupButton()
    setupCountLabel()

    count = 0
  }

  func setupButton() {
    setImage(R.image.filterTemplate(), for: .normal)
  }

  func setupCountLabel() {
    countLabel = UILabel()
    countLabel.backgroundColor = .systemBlue
    countLabel.font = .systemFont(ofSize: 12)
    countLabel.isHidden = true
    countLabel.textColor = .white
    countLabel.textAlignment = .center
    countLabel.layer.cornerRadius = labelWidth / 2
    countLabel.layer.masksToBounds = true
    countLabel.layer.borderWidth = 1
    countLabel.layer.borderColor = UIColor.white.cgColor

    addSubview(countLabel)
    countLabel.autoSetDimensions(to: CGSize(width: labelWidth, height: labelWidth))
    countLabel.autoPinEdge(toSuperviewEdge: .trailing, withInset: 4)
    countLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 4)
  }
}

// MARK: - Helpers

private extension FilterCountButton {
  func didSetCount() {
    let isHidden = (count == 0)

    countLabel.text = count.description
    countLabel.isHidden = isHidden
    tintColor = isHidden ? R.color.deprecated_Grey_B2B2B2()! : R.color.deprecated_Blue_9dbcf7()!
  }
}
