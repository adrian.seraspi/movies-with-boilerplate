//
//  UICollectionViewCell+Static.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
  static var reuseIdentifier: String {
    String(describing: self)
  }
}
