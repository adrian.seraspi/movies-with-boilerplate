//
//  AppDelegate+RootViewController.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import NSObject_Rx
import RxSwift
import UIKit

/// Extension for RootViewController Management
///
/// Notes:
/// - This fits perfectly with the Coordinator Pattern as the AppCoordinator type.
///
extension AppDelegate {
  func updateRootViewController(isFromAppLaunch: Bool = false) {
    presentMovieList()
  }
  
  func presentMovieList() {
    let movieListVC = R.storyboard.movieList.movieListController()!
    let nc = NavigationController(rootViewController: movieListVC)
    window?.setRootViewControllerAnimated(nc)
  }
}
