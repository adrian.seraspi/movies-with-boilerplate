//
//  AppDelegate+Appearance.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import MaterialComponents.MDCSnackbarManager
import SVProgressHUD
import UIKit

// All UIAppearance configurations should all go in here.

// MARK: - Appearance

extension AppDelegate {
  func loadAppearancePreferences() {
    loadNavBarAppearancePrefs()
    loadBarButtonItemAppearancePrefs()
    loadTableViewCellAppearancePrefs()

    applySVProgressHUDTheme()
    applyMDCSnackbarTheme()
  }

  private func loadNavBarAppearancePrefs() {
    let navBar = UINavigationBar.appearance(whenContainedInInstancesOf: [
      NavigationController.self
    ])

    navBar.barTintColor = .white
    navBar.tintColor = .black
    navBar.titleTextAttributes = [
      NSAttributedString.Key.foregroundColor: UIColor.black as Any,
      NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .semibold) as Any
    ]
  }

  private func loadBarButtonItemAppearancePrefs() {
    let barButton = UIBarButtonItem.appearance(whenContainedInInstancesOf: [
      UINavigationController.self
    ])
    barButton.setTitleTextAttributes(
      [
        NSAttributedString.Key.foregroundColor: Styles.Colors.primaryColor,
        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .semibold)
      ],
      for: .normal
    )
  }

  private func loadTableViewCellAppearancePrefs() {
    /*
     let cell = UITableViewCell.appearance(whenContainedInInstancesOf: [...])
     cell.tintColor = ...
     */
  }
}

// MARK: - SVProgressHUD

private extension AppDelegate {
  func applySVProgressHUDTheme() {
    SVProgressHUD.setDefaultAnimationType(.native)
    SVProgressHUD.setMinimumSize(CGSize(width: 86, height: 86))

    SVProgressHUD.setDefaultStyle(.custom)
    SVProgressHUD.setBackgroundColor(R.color.deprecated_Grey_F7F7F7()!)
    SVProgressHUD.setBackgroundLayerColor(R.color.deprecated_Grey_F7F7F7()!)
  }
}

// MARK: - MDCSnackbar

private extension AppDelegate {
  func applyMDCSnackbarTheme() {
    MDCSnackbarManager.messageFont = .systemFont(ofSize: 17)
    MDCSnackbarManager.messageTextColor = .white
  }
}
