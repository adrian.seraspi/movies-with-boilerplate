//
//  AppDelegate.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import AlamofireNetworkActivityIndicator
import AlamofireNetworkActivityLogger
import FBSDKCoreKit
import Firebase
import GoogleSignIn
import IQKeyboardManagerSwift
import Stripe
import UIKit
import DropDown
import CoreData
class AppDelegate: UIResponder, UIApplicationDelegate {
  // swiftlint:disable:next force_cast
  static var shared: AppDelegate { UIApplication.shared.delegate as! AppDelegate }
  
  var window: UIWindow?

  override init() {}

  func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil
  ) -> Bool {
    if !UIApplication.isRunningTests {
      FirebaseApp.configure()
    }

    App.shared.bootstrap(with: application,
                         launchOptions: launchOptions,
                         persistentContainer: self.persistentContainer)

    Stripe.setDefaultPublishableKey(App.shared.config.secrets.stripePublishKey)
    DropDown.startListeningToKeyboard()
    
    #if DEBUG
      NetworkActivityLogger.shared.level = .debug
      NetworkActivityLogger.shared.startLogging()
    #endif

    IQKeyboardManager.shared.enable = true

    NetworkActivityIndicatorManager.shared.isEnabled = true


    // Facebook Application Delegate
    //
    // Requires the ff info.plist keys:
    // - FacebookAppID
    // - FacebookDisplayName
    // - LSApplicationQueriesSchemes
    //
    // Check facebook documentation for updated keys:
    // - https://developers.facebook.com/docs/facebook-login/ios
    ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)

    window = UIWindow(frame: UIScreen.main.bounds)
    window?.backgroundColor = Styles.Colors.windowBackground
    window?.tintColor = Styles.Colors.windowTint
    updateRootViewController(isFromAppLaunch: true)
    loadAppearancePreferences()
    window?.makeKeyAndVisible()
    
    return true
  }

  func applicationDidBecomeActive(_ application: UIApplication) {}

  func applicationDidEnterBackground(_ application: UIApplication) {}

  func applicationWillResignActive(_ application: UIApplication) {}

  func applicationWillEnterForeground(_ application: UIApplication) {}

  func applicationWillTerminate(_ application: UIApplication) {}
  
  // MARK: - Core Data stack

  lazy var persistentContainer: NSPersistentContainer = {
      /*
       The persistent container for the application. This implementation
       creates and returns a container, having loaded the store for the
       application to it. This property is optional since there are legitimate
       error conditions that could cause the creation of the store to fail.
      */
      let container = NSPersistentContainer(name: "Movies")
      container.loadPersistentStores(completionHandler: { (_, error) in
          if let error = error as NSError? {
              // Replace this implementation with code to handle the error appropriately.
              // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
               
              /*
               Typical reasons for an error here include:
               * The parent directory does not exist, cannot be created, or disallows writing.
               * The persistent store is not accessible, due to permissions or data protection when the device is locked.
               * The device is out of space.
               * The store could not be migrated to the current model version.
               Check the error message to determine what the actual problem was.
               */
              fatalError("Unresolved error \(error), \(error.userInfo)")
          }
      })
      return container
  }()

  // MARK: - Core Data Saving support

  func saveContext () {
      let context = persistentContainer.viewContext
      if context.hasChanges {
          do {
              try context.save()
          } catch {
              // Replace this implementation with code to handle the error appropriately.
              // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
              let nserror = error as NSError
              fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
          }
      }
  }
  
}

// MARK: - Inter-App Communication

extension AppDelegate {
  func application(
    _ app: UIApplication,
    open url: URL,
    options: [UIApplication.OpenURLOptionsKey: Any] = [:]
  ) -> Bool {
    // Handle Facebook URL Scheme
    if ApplicationDelegate.shared.application(app, open: url, options: options) {
      return true
    }

    // Handle Google URL Scheme
    if GIDSignIn.sharedInstance().handle(url) {
      return true
    }

    return false
  }
}
