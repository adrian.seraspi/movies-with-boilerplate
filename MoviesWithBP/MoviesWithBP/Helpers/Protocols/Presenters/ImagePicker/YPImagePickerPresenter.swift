//
//  YPImagePickerPresenter.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation
import UIKit

import YPImagePicker

class YPImagePickerPresenter: ImagePickerPresenterProtocol {
  var onImagesPick: SingleResult<[UIImage]>?
  var onError: SingleResult<Error>?
  
  weak var anchorController: UIViewController?
  
  private var config: YPImagePickerConfiguration!
  private var imagePicker: YPImagePicker!
  
  init(config: YPImagePickerConfiguration = .default) {
    self.config = config
    self.imagePicker = YPImagePicker(configuration: config)
  }
}

extension YPImagePickerPresenter {
  func presentPicker() {
    imagePicker.didFinishPicking { [weak self] items, _ in
      guard let self = self else { return }
      self.imagePicker.dismiss(animated: true)
      if items.isEmpty { return }
      let images: [UIImage] = items.compactMap {
        guard case let .photo(photo) = $0 else { return nil }
        return photo.image
      }
      self.onImagesPick?(images)
    }
    
    anchorController?.present(imagePicker, animated: true)
  }
  
  
  func presentPicker(maxNumberOfItems: Int) {
    config.library.maxNumberOfItems = maxNumberOfItems
    imagePicker = YPImagePicker(configuration: config)
    presentPicker()
  }
}

extension YPImagePickerConfiguration {
  static var `default`: YPImagePickerConfiguration {
    var config = YPImagePickerConfiguration()
    config.showsPhotoFilters = false
    config.startOnScreen = YPPickerScreen.library
    config.library.defaultMultipleSelection = true
    config.library.mediaType = YPlibraryMediaType.photo
    config.library.skipSelectionsGallery = true
    return config
  }
}
