//
//  DefaultSnackbarSuccessInfo.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

struct DefaultSnackbarSuccessInfo: InfoProtocol {
  let message: String
}

extension DefaultSnackbarSuccessInfo {
  var foregroundColor: UIColor { .white }
  var backgroundColor: UIColor { Styles.Colors.primaryColor }
  var action: InfoAction? { .snackbarDismiss }
}
