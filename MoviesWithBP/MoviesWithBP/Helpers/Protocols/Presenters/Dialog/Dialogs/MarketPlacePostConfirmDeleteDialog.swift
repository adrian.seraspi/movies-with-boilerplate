//
//  MarketPlacePostConfirmDeleteDialog.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

struct MarketPlacePostConfirmDeleteDialog: DialogProtocol {
  let onConfirm: VoidResult
  
  init(onConfirm: @escaping VoidResult) {
    self.onConfirm = onConfirm
  }
}

extension MarketPlacePostConfirmDeleteDialog {
  var title: String? { S.dialogsDeleteMarketPlacePostTitle() }
  var message: String? { nil }
  
  var cancelOption: DialogOption? { nil }
  var positiveOption: DialogOption? {
    DialogOption(
      title: S.dialogsDeleteMarketPlacePostButtonsNegative()
    )
  }
  var negativeOption: DialogOption? {
    DialogOption(
      title: S.dialogsDeleteMarketPlacePostButtonsPositive(),
      onSelect: onConfirm
    )
  }
}
