//
//  Styles.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

@available(*, deprecated, message: "Use Themes instead")
struct Styles {
  struct Colors {}
}

extension Styles.Colors {
  static let primaryColor = R.color.deprecated_Blue_2C66D5()!
  static let secondaryColor = R.color.deprecated_Blue_3497FD()!
  static let accentColor = R.color.deprecated_Blue_3497FD()!

  static let textColorLight = UIColor.black // for light colored backgrounds
  static let textColorDark = UIColor.white // for dark colored backgrounds

  static let primaryTextColor = Self.textColorLight
  static let secondaryTextColor = R.color.deprecated_Grey_78849E()!
  static let linkTextColor = Self.primaryColor

  static let windowTint = Self.primaryColor
  static let windowBackground = UIColor.white
  static let viewControllerBackground = UIColor.white

  /// Colors for Form elements like Button, TextField, etc.
  struct Form {}
}

extension Styles.Colors.Form {
  static let normalColor = R.color.deprecated_Grey_78849E()!
  static let focusedColor = Styles.Colors.secondaryColor
  static let errorColor = R.color.deprecated_Red_FF404E()!

  static let placeholderTextColor = R.color.deprecated_Grey_78849E()!

  struct Button {}
}

extension Styles.Colors.Form.Button {
  static let primaryGradient = [
    R.color.deprecated_Blue_3ACCE1()!,
    R.color.deprecated_Blue_2C66D5()!
  ]
}
