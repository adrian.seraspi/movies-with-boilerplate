//
//  CreatePostInputValidator.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct CreatePostInputValidator: InputValidator {
  typealias ValidateMethod = (Inputs) -> Result<ValidInputs, ValidationError>

  static func validate(_ inputs: Inputs) -> Result<ValidInputs, ValidationError> {
    debugLog("inputs: \(inputs)")
    guard let imageData = inputs.imageData else {
      return .failure(.requiredImage)
    }
    guard let description = inputs.description, !description.isEmpty else {
      return .failure(.requiredDescription)
    }
    let validInputs = ValidInputs(
      imageData: imageData,
      description: description
    )
    return .success(validInputs)
  }
}

// MARK: - Type Declarations

extension CreatePostInputValidator {
  typealias Inputs = (
    imageData: Data?,
    description: String?
  )

  typealias ValidInputs = (
    imageData: Data,
    description: String
  )

  enum ValidationError: LocalizedError {
    case requiredImage, requiredDescription

    var errorDescription: String? {
      switch self {
      case .requiredImage:
        return S.createPostErrorImageRequired()
      case .requiredDescription:
        return S.createPostErrorDescriptionRequired()
      }
    }
  }
}
