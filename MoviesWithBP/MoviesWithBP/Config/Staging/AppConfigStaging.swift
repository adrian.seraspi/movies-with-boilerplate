//
//  AppConfigStaging.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct AppConfigStaging: AppConfigProtocol {
  var baseUrl: String { "https://itunes.apple.com" }

  var secrets: AppSecretsProtocol { AppSecretsStaging() }
}
