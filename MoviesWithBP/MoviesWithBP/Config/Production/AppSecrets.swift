//
//  AppSecrets.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol AppSecretsProtocol {
  var googleClientID: String { get }
  var stripePublishKey: String { get }
}

struct AppSecrets: AppSecretsProtocol {
  var googleClientID: String { "$(PROD_GOOGLE_CLIENT_ID)" }
  var stripePublishKey: String { "$(PROD_STRIPE_PUBLISH_KEY)" }
}
