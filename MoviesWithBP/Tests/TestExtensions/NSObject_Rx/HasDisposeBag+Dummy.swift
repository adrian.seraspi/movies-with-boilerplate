//
//  HasDisposeBag+Dummy.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import NSObject_Rx

class DummyHasDisposeBag: HasDisposeBag {}
