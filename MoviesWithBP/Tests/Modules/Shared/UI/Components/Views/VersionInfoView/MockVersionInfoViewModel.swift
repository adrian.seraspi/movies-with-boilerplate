//
//  MockVersionInfoViewModel.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MoviesWithBP

class MockVersionInfoViewModel: VersionInfoViewModelProtocol {
  var versionText: String = ""
}
