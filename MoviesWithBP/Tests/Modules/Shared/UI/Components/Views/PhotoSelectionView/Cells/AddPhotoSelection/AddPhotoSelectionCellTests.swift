//
//  AddPhotoSelectionCellTests.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Quick
import Nimble

@testable import MoviesWithBP

class AddPhotoSelectionCellTests: QuickSpec {
  override func spec() {
    describe("AddPhotoSelectionCell") {
      // No tests so far
    }
  }
}
