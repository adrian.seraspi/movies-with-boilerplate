//
//  MovieServiceTests.swift
//  Tests
//
//  Created by Adrian Jun Seraspi on 5/14/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MoviesWithBP

class MovieServiceTests: QuickSpec {
  override func spec() {
    describe("MovieService") {
      var sut: MovieService!
      var api: MockMovieAPI!
      var local: MockLocalDatabase!
      
      beforeEach {
        api = MockMovieAPI()
        local = MockLocalDatabase()
        sut = MovieService(api: api, localDb: local)
      }
      
      afterEach {
        api = nil
        sut = nil
        local = nil
      }
      
      it("should pass correct params and call api.getSearchMovies once on fetchSearchMovie") {
        expect(local.deleteAllMoviesCallCount).to(equal(0))
        expect(local.saveAllMoviesCallCount).to(equal(0))
        expect(local.saveAllMoviesList).to(beNil())
        
        expect(api.getSearchMoviesQuery).to(beNil())
        expect(api.getSearchMoviesCountry).to(beNil())
        
        sut.fetchSearchMovie(query: "Superman",
                             country: "au",
                             onSuccess: DefaultClosure.singleResult(),
                             onError: DefaultClosure.singleResult())
        
        expect(api.getSearchMoviesQuery).to(equal("Superman"))
        expect(api.getSearchMoviesCountry).to(equal("au"))
        
        expect(local.deleteAllMoviesCallCount).to(equal(1))
        expect(local.saveAllMoviesCallCount).to(equal(1))
        expect(local.saveAllMoviesList).toNot(beNil())
      }
    }
  }
}
