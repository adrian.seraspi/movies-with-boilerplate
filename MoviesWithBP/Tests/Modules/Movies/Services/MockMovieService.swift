//
//  MockMovieService.swift
//  Tests
//
//  Created by Adrian Jun Seraspi on 5/14/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MoviesWithBP

class MockMovieService: MovieServiceProtocol {
  
  var errorToReturn: Error?
  
  private(set) var fetchSearchMovieQuery: String?
  private(set) var fetchSearchMovieCountry: String?
  
  func fetchSearchMovie(query: String,
                        country: String,
                        onSuccess: @escaping SingleResult<[MainMovies]>,
                        onError: @escaping ErrorResult) {
    fetchSearchMovieQuery = query
    fetchSearchMovieCountry = country
    
    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess([])
    }
  }
  
  func getMoviesFromLocal(onSuccess: @escaping SingleResult<[MainMovies]>, onError: @escaping ErrorResult) {
    
  }
}
