//
//  DummyMovies.swift
//  Tests
//
//  Created by Adrian Jun Seraspi on 5/17/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MoviesWithBP

extension MainMovies {
  init(testId: Int = 0,
       testImageUrl: String = "https://images.google.com",
       testPreviewUrl: String = "https://youtube.com/star",
       testTrackName: String = "Star wars",
       testTrackPrice: Double = 40.00,
       testCurrency: String = "AUD",
       testGenre: String = "Sci-fi",
       testLongDescription: String = "A star wars long description"
  ) {
    self.init(id: testId,
              imageUrl: testImageUrl,
              previewUrl: testPreviewUrl,
              trackName: testTrackName,
              trackPrice: testTrackPrice,
              currency: testCurrency,
              genre: testGenre,
              longDescription: testLongDescription)
  }
}
