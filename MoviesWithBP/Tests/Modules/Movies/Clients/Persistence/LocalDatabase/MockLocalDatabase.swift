//
//  MockLocalDatabase.swift
//  Tests
//
//  Created by Adrian Jun Seraspi on 5/19/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MoviesWithBP

class MockLocalDatabase: LocalDatabaseProtocol {
  var errorToReturn: Error?
  
  private(set) var saveAllMoviesList: [MainMovies]?
  private(set) var saveAllMoviesCallCount: Int = 0
  private(set) var getAllMoviesCallCount: Int = 0
  private(set) var deleteAllMoviesCallCount: Int = 0
  
  func saveAllMovies(list: [MainMovies]) {
    saveAllMoviesList = list
    saveAllMoviesCallCount += 1
  }
    
  func getAllMovies(onSuccess: @escaping SingleResult<[MainMovies]>,
                    onError: @escaping ErrorResult) {
    getAllMoviesCallCount += 1
    
    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess([])
    }
  }
  
  func deleteAllMovies() {
    deleteAllMoviesCallCount += 1
  }
}
