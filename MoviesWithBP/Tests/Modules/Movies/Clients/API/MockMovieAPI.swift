//
//  MockMovieAPI.swift
//  Tests
//
//  Created by Adrian Jun Seraspi on 5/14/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MoviesWithBP

class MockMovieAPI: MoviesAPI {
  var errorToReturn: Error?
  
  private(set) var getSearchMoviesQuery: String?
  private(set) var getSearchMoviesCountry: String?
  
  func getSearchMovies(query: String, country: String, onSuccess: @escaping SingleResult<[ITunesMovieList]>, onError: @escaping ErrorResult) -> RequestProtocol {
    self.getSearchMoviesQuery = query
    self.getSearchMoviesCountry = country
    
    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess([])
    }
    
    return DummyRequest()
  }
}
