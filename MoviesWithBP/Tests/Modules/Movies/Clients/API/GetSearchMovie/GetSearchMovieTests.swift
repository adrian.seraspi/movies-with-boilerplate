//
//  GetSearchMovieTests.swift
//  Tests
//
//  Created by Adrian Jun Seraspi on 5/14/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Quick
import Nimble

@testable import MoviesWithBP

class GetSearchMovieTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("GetSearchMovie") {
      var apiResponse: APIResponse!
      var data: [ITunesMovieList]!
      
      afterEach {
        apiResponse = nil
        data = nil
      }
      
      context("when decoding status 200 response") {
        beforeEach {
          apiResponse = self.decodeResponseValue(statusCode: .ok)
        }
        
        it("should have non-nil decoded response") {
          expect(apiResponse).toNot(beNil())
        }
        
        it("should have 200 status code") {
          data = apiResponse.decodedValue()
          
          expect(data).toNot(beNil())
        }
      }
    }
  }
}
