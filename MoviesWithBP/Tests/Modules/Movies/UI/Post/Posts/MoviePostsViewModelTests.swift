//
//  MovieListViewModelTests.swift
//  Tests
//
//  Created by Adrian Jun Seraspi on 5/14/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MoviesWithBP

class MoviePostsViewModelTests: QuickSpec {
  override func spec() {
    describe("MoviePostsViewModel") {
      var sut: MoviePostsViewModel!
      var service: MockMovieService!
      
      beforeEach {
        service = MockMovieService()
        sut = MoviePostsViewModel(service: service)
      }
      
      afterEach {
        service = nil
        sut = nil
      }
      
      it("should call service.fetchSearchMovie once on searchMoviePosts") {
        expect(service.fetchSearchMovieQuery).to(beNil())
        expect(service.fetchSearchMovieCountry).to(beNil())
        
        sut.searchMoviePosts(query: "Superman",
                             onSuccess: DefaultClosure.voidResult(),
                             onFailed: DefaultClosure.singleResult())

        
        expect(service.fetchSearchMovieQuery).to(equal("Superman"))
        expect(service.fetchSearchMovieCountry).to(equal("au"))
      }
    }
  }
}


