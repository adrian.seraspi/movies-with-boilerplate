//
//  MoviePostsControllerTests.swift
//  Tests
//
//  Created by Adrian Jun Seraspi on 5/17/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MoviesWithBP

class MoviePostsControlllerTests: QuickSpec {
  override func spec() {
    describe("MoviePostsControlllerTests") {
      var sut: MoviePostsController!
      var viewModel: MockMoviePostsViewModel!
      
      beforeEach {
        viewModel = MockMoviePostsViewModel()
        sut = R.storyboard.movieListPost.moviePostsController()
        sut.viewModel = viewModel
      }
      
      afterEach {
        viewModel = nil
        sut = nil
      }
      
      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }
        
        it("should have non-nil outlets") {
          expect(sut.refreshView).toNot(beNil())
          expect(sut.tableView).toNot(beNil())
        }
        
        it("should set tableView delegate and dataSource to self") {
          expect(sut.tableView.delegate).to(be(sut))
          expect(sut.tableView.dataSource).to(be(sut))
        }
        
        it("should call viewModel.searchMoviePosts once") {
          expect(viewModel.searchMovieQuery).to(beNil())
          expect(viewModel.searchMoviePostsCallCount).to(equal(0))
          
          viewModel.searchMoviePosts(query: "Superman",
                                     onSuccess: DefaultClosure.voidResult(),
                                     onFailed: DefaultClosure.singleResult())
          
          expect(viewModel.searchMovieQuery).to(equal("Superman"))
          expect(viewModel.searchMoviePostsCallCount).to(equal(1))
        }
        
        it("should call viewModel.generatePostDetailsVM(at:) once in indexPath select") {
          expect(viewModel.generateMovieDetailsCallCount).to(equal(0))
          expect(viewModel.generateMovieDetailsVMIndex).to(beNil())
          
          sut.tableView.tap(row: 0)
          
          expect(viewModel.generateMovieDetailsCallCount).to(equal(1))
          expect(viewModel.generateMovieDetailsVMIndex).to(equal(0))
        }
        
        it("should call viewModel.loadPersistenceDataCallCount once after view is loaded") {
          expect(viewModel.loadPersistenceDataCallCount).to(equal(1))
        }
      }
    }
  }
}
