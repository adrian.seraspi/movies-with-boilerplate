//
//  MockMoviePostsViewModel.swift
//  Tests
//
//  Created by Adrian Jun Seraspi on 5/17/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MoviesWithBP

class MockMoviePostsViewModel: MoviePostsViewModelProtocol {
  var onRefresh: BoolResult?
  
  var datasource: [MovieCellViewModelProtocol] = []
  var errorToReturn: Error?
  
  private(set) var searchMovieQuery: String?
  private(set) var searchMoviePostsCallCount = 0
  private(set) var generateMovieDetailsCallCount = 0
  private(set) var generateMovieDetailsVMIndex: Int?
  private(set) var loadPersistenceDataCallCount: Int = 0
}

extension MockMoviePostsViewModel {
  func searchMoviePosts(
    query: String,
    onSuccess: @escaping VoidResult,
    onFailed: @escaping ErrorResult
  ) {
    self.searchMovieQuery = query
    self.searchMoviePostsCallCount += 1
    
    if let e = errorToReturn {
      onFailed(e)
    } else {
      onSuccess()
    }
  }
  
  func loadPersistenceData(onSuccess: @escaping VoidResult, onFailed: @escaping ErrorResult) {
    self.loadPersistenceDataCallCount += 1
    
    if let e = errorToReturn {
      onFailed(e)
    } else {
      onSuccess()
    }
  }
  
  func generateMovieDetailsVM(at index: Int) -> MovieDetailsViewModelProtocol {
    self.generateMovieDetailsCallCount += 1
    self.generateMovieDetailsVMIndex = index
    return MockMovieDetailsViewModel()
  }
}
