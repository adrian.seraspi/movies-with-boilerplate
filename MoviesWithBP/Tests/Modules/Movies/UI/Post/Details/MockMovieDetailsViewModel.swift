//
//  MockMovieDetailsViewModel.swift
//  Tests
//
//  Created by Adrian Jun Seraspi on 5/17/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MoviesWithBP

class MockMovieDetailsViewModel: MovieDetailsViewModelProtocol {
  var title: String = ""
  var genre: String = ""
  var longDescription: String = ""
  var previewUrl: URL?
  var imageUrl: URL?
  var hasPreview: Bool = false
}
