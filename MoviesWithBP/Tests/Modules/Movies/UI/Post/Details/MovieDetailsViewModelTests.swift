//
//  MovieDetailsViewModelTests.swift
//  Tests
//
//  Created by Adrian Jun Seraspi on 5/17/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MoviesWithBP

class MovieDetailsViewModelTests: QuickSpec {
  override func spec() {
    describe("MovieDetailsViewModelTests") {
      var sut: MovieDetailsViewModel!
      var mainMovies: MainMovies!
      
      beforeEach {
        mainMovies = MainMovies(testId: 0)
        sut = MovieDetailsViewModel(movie: mainMovies)
      }
      
      afterEach {
        sut = nil
        mainMovies = nil
      }
      
      it("should return correct values") {
        expect(sut.genre).to(equal(mainMovies.genre))
        expect(sut.hasPreview).to(equal(true))
        expect(sut.imageUrl).to(equal(URL(string: mainMovies.imageUrl)))
        expect(sut.longDescription).to(equal(mainMovies.longDescription))
        expect(sut.previewUrl).to(equal(URL(string: mainMovies.previewUrl)))
        expect(sut.title).to(equal(mainMovies.trackName))
      }
    }
  }
}
