//
//  MovieDetailsControllerTests.swift
//  Tests
//
//  Created by Adrian Jun Seraspi on 5/17/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MoviesWithBP

class MovieDetailsControllerTests: QuickSpec {
  override func spec() {
    describe("MovieDetailsControllerTests") {
      var sut: MovieDetailsController!
      var viewModel: MockMovieDetailsViewModel!
      
      beforeEach {
        viewModel = MockMovieDetailsViewModel()
        sut = R.storyboard.movieListPost.movieDetailsController()
        sut.viewModel = viewModel
      }
      
      afterEach {
        viewModel = nil
        sut = nil
      }
      
      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }
        
        it("should have non-nil outlets") {
          expect(sut.genreLabel).toNot(beNil())
          expect(sut.loadingIndicator).toNot(beNil())
          expect(sut.longDescriptionLabel).toNot(beNil())
          expect(sut.previewContainerView).toNot(beNil())
        }
      }
    }
  }
}
