//
//  MockMovieListMainViewModel.swift
//  Tests
//
//  Created by Adrian Jun Seraspi on 5/19/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MoviesWithBP

class MockMovieListMainViewModel: MovieListMainViewModelProtocol {
  var persistSearch: String = ""
}
