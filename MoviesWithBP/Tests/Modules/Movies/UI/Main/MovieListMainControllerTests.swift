//
//  MovieLstMainControllerTests.swift
//  Tests
//
//  Created by Adrian Jun Seraspi on 5/17/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MoviesWithBP

class MovieListMainControllerTests: QuickSpec {
  override func spec() {
    describe("MovieListMainControllerTests") {
      var sut: MovieListMainController!
      
      beforeEach {
        sut = R.storyboard.movieList.movieListController()
      }
      
      afterEach {
        sut = nil
      }
      
      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }
        
        it("should have non-nil outlets") {
          expect(sut.searchBar).toNot(beNil())
        }
      }
    }
  }
}
