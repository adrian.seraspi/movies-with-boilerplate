//
//  MovieListMainViewModelTests.swift
//  Tests
//
//  Created by Adrian Jun Seraspi on 5/19/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import Nimble
import Quick

@testable import MoviesWithBP

class MovieListMainVewModelTests: QuickSpec {
  override func spec() {
    describe("MovieListMainVewModelTests") {
      var sut: MovieListMainViewModel!
      var repo: MockMoviePersistRepository!
      
      
      beforeEach {
        repo = MockMoviePersistRepository()
        sut = MovieListMainViewModel(repo: repo)
      }
      
      afterEach {
        repo = nil
        sut = nil
      }
      
      it("should call repo.getSearch once on persistSearch") {
        repo.search = "123"
        expect(repo.getSearchCallCount).to(equal(0))
        
        let expectedSearch = sut.persistSearch
        expect(repo.getSearchCallCount).to(equal(1))
        expect(expectedSearch).to(equal("123"))
      }
    }
  }
}
