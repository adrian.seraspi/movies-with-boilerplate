//
//  MockMoviePersistRepository.swift
//  Tests
//
//  Created by Adrian Jun Seraspi on 5/19/21.
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MoviesWithBP

class MockMoviePersistRepository: MoviePersistRepositoryProtocol {
  private(set) var addNewEntryQuery: String?
  private(set) var getSearchCallCount: Int = 0
  private(set) var addNewEntryQueryCallCount: Int = 0
  var search = "test"
  
  func addNewEntry(_ query: String) {
    addNewEntryQueryCallCount += 1
    addNewEntryQuery = query
  }
  
  func getSearch() -> String {
    getSearchCallCount += 1
    return search
  }
}
