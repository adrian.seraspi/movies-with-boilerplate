//
//  DummyAPIClientFailedRequestInfo.swift
//  MoviesWithBP
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MoviesWithBP

extension APIClientFailedRequestInfo {
  init(
    testStatus: HTTPStatusCode = .accepted,
    testMessage: String = "",
    testErrorCode: APIErrorCode = .default
  ) {
    self.init(
      status: testStatus,
      message: testMessage,
      errorCode: testErrorCode
    )
  }
}
